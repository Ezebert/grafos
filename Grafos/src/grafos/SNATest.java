package grafos;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SNATest 
{
	// Instancia de test
	private Grafo _test;

	@Before
	public void construirGrafoDeTest()
	{
		_test = new Grafo(11);
		_test.agregarArista(0, 1);
		_test.agregarArista(0, 2);
		_test.agregarArista(1, 2);
		_test.agregarArista(1, 3);
		_test.agregarArista(2, 4);
		_test.agregarArista(4, 3);
		_test.agregarArista(3, 5);
		_test.agregarArista(7, 8);
		_test.agregarArista(7, 9);
		_test.agregarArista(7, 10);
		_test.agregarArista(8, 9);
		_test.agregarArista(9, 10);
	}

	@Test
	public void vecinosDeVecinosTest()
	{
		SNA sna = new SNA(_test);
		Assert.setsIguales(new int[] {3, 4}, sna.vecinosDeVecinos(0));
		Assert.setsIguales(new int[] {0, 1, 5}, sna.vecinosDeVecinos(4));
		Assert.setsIguales(new int[] {}, sna.vecinosDeVecinos(6));
		Assert.setsIguales(new int[] {}, sna.vecinosDeVecinos(7));
	}

	@Test
	public void vecinosEnComunTest()
	{
		SNA sna = new SNA(_test);
		assertEquals(1, sna.vecinosEnComun(0, 3).size());
		assertEquals(2, sna.vecinosEnComun(2, 3).size());
		assertEquals(0, sna.vecinosEnComun(0, 5).size());
		assertEquals(0, sna.vecinosEnComun(6, 7).size());
	}

	@Test
	public void recomendarAmigoTest()
	{
		SNA sna = new SNA(_test);
		assertTrue(2 == sna.recomendarAmigo(3));
		assertTrue(3 == sna.recomendarAmigo(2));
	}
	
	@Test
	public void recomendarAmigoInexistenteTest()
	{
		SNA sna = new SNA(_test);
	
		assertNull(sna.recomendarAmigo(6));
		assertNull(sna.recomendarAmigo(7));
	}
}
