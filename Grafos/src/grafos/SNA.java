package grafos;

import java.util.HashSet;
import java.util.Set;

public class SNA // Social Network Analysis
{
	private Grafo _grafo;
	
	public SNA(Grafo grafo)
	{
		_grafo = grafo;
	}
	
	// Retorna el vertice a distancia 2 con mayor cantidad
	// de vecinos en comun con "usuario", en O(n�) promedio
	public Integer recomendarAmigo(int usuario)
	{
		Integer ret = null;
		int max = Integer.MIN_VALUE;
		
		for(Integer candidato: vecinosDeVecinos(usuario))
		{
			int enComun = vecinosEnComun(usuario, candidato).size();
			if( enComun > max )
			{
				ret = candidato;
				max = enComun;
			}
		}
		
		return ret;
	}

	// Conjunto de vertices a distancia 2 de "usuario", en O(n�) promedio
	Set<Integer> vecinosDeVecinos(int usuario)
	{
		Set<Integer> ret = new HashSet<Integer>();
		for(Integer vecino: _grafo.vecinos(usuario))
			ret.addAll(_grafo.vecinos(vecino));
		
		ret.remove(usuario);
		ret.removeAll(_grafo.vecinos(usuario));
		
		return ret;
	}
		
	// Vecinos en comun entre u y v, en O(n) promedio
	Set<Integer> vecinosEnComun(int u, int v)
	{
		Set<Integer> ret = _grafo.vecinos(u);
		ret.retainAll(_grafo.vecinos(v));
		
		return ret;
	}	
}












