package grafos;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class BFS
{
	// El grafo sobre el que trabaja el algoritmo
	private Grafo _grafo;
	
	// Auxiliares durante la ejecución
	private ArrayList<Integer> L;
	private boolean[] marcados;
	
	// Constructor
	public BFS(Grafo grafo)
	{		
		_grafo = grafo;
	}

	// Vertices alcanzables desde el vertice inicial
	public Set<Integer> alcanzables(int inicial)
	{
		inicializar(inicial);
		while( L.size() > 0 )
		{
			int i = L.get(0);
			marcados[i] = true;
	
			agregarVecinosPendientes(i);
			L.remove(0);
		}

		return setMarcados();
	}

	// Inicializa las variables auxiliares del algoritmo
	private void inicializar(int inicial)
	{
		// Pendientes de visitar
		L = new ArrayList<Integer>();
		L.add(inicial);
		
		// Visitados
		marcados = new boolean[_grafo.vertices()];
	}

	// Agregar a L los vecinos no marcados de i
	private void agregarVecinosPendientes(int i)
	{
		for(Integer v: _grafo.vecinos(i))
		{
			if( !marcados[v] && !L.contains(v) )
				L.add(v);
		}
	}
	
	// Pone los elementos en true en un set
	private Set<Integer> setMarcados()
	{
		Set<Integer> ret = new HashSet<Integer>();
		for(int i=0; i<marcados.length; ++i)
		{
			if( marcados[i] == true )
				ret.add(i);
		}

		return ret;
	}

	// Casos de test
	public static void main(String[] args)
	{
		Grafo grafo = new Grafo(6);
		grafo.agregarArista(1, 4);
		grafo.agregarArista(1, 2);
		grafo.agregarArista(1, 3);
		grafo.agregarArista(2, 5);
		grafo.agregarArista(5, 0);
		grafo.agregarArista(0, 3);
		grafo.agregarArista(2, 3);
		
		BFS bfs = new BFS(grafo);
		System.out.println(bfs.alcanzables(4).size() == 6);
		
		grafo.eliminarArista(1, 2);
		grafo.eliminarArista(1, 3);

		System.out.println(bfs.alcanzables(4).size() == 2);
		System.out.println(bfs.alcanzables(2).size() == 4);

		grafo.eliminarArista(1, 4);
		System.out.println(bfs.alcanzables(4).size() == 1);
	}
}




